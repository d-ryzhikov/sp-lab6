#include <Windows.h>
#define _USE_MATH_DEFINES
#include <cmath>

#include "resource.h"

LPCWSTR windowClassName = L"sp_lab6";

const int WINDOW_WIDTH = 600;
const int WINDOW_HEIGHT = 400;

const int CIRCLE_RADIUS = 10;

HANDLE threads [6];

struct ThreadData
{
    HWND hwnd;
    RECT rect;
};

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

DWORD WINAPI ThreadProc(LPVOID lpParameter);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASSEX wc;
    HWND hwnd;
    MSG msg;

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = 1;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
    wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wc.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW);
    wc.lpszMenuName = MAKEINTRESOURCE(IDR_MENU);
    wc.lpszClassName = windowClassName;
    wc.hIconSm = nullptr;

    if (!RegisterClassEx(&wc))
    {
        MessageBox(nullptr, L"Couldn't register the Window Class.", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return -1;
    }

    hwnd = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        windowClassName,
        L"������������ ������ �6",
        WS_OVERLAPPEDWINDOW &~WS_MAXIMIZEBOX,
        CW_USEDEFAULT, CW_USEDEFAULT, WINDOW_WIDTH, WINDOW_HEIGHT,
        nullptr, nullptr, hInstance,nullptr 
    );

    if (hwnd == nullptr)
    {
        MessageBox(nullptr, L"Couldn't create the Window Class.", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return -1;
    }

    RECT rect;
    GetClientRect(hwnd, &rect);

    int block_width = rect.right / 3;
    int block_height = rect.bottom / 2;

    ThreadData* data = reinterpret_cast<ThreadData*>(
        HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(ThreadData) * 6)
    );

    data[0].hwnd = data[1].hwnd = data[2].hwnd = data[3].hwnd = data[4].hwnd =
        data[5].hwnd = hwnd;

    data[0].rect = {
        0, 0, block_width, block_height,
    };
    data[1].rect = {
        block_width, 0, block_width * 2, block_height,
    };
    data[2].rect = {
       block_width * 2, 0, block_width * 3, block_height,
    };

    data[3].rect = {
        rect.left, block_height, block_width, block_height * 2,
    };
    data[4].rect = {
        rect.left + block_width, block_height, block_width * 2, block_height * 2,
    };
    data[5].rect = {
        rect.left + block_width * 2, block_height, block_width * 3, block_height * 2,
    };

    for (int i = 0; i < 6; ++i)
    {
        threads[i] = CreateThread(nullptr, 0, ThreadProc, &data[i],
            CREATE_SUSPENDED, nullptr);
        if (threads[i] == nullptr)
        {
            MessageBox(nullptr, L"Couldn't create a thread.", L"Error!",
                MB_ICONEXCLAMATION | MB_OK);
        }
    }

    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    while (GetMessage(&msg, nullptr, 0, 0) > 0)
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    switch (Msg)
    {
        case WM_CREATE:
            break;
        case WM_COMMAND:
            switch (LOWORD(wParam))
            {
                case ID_MENU_START:
                    for (int i = 0; i < 6; ++i)
                    {
                        if (ResumeThread(threads[i]) == -1)
                        {
                            MessageBox(nullptr, L"Couldn't resume a thread.",
                                L"Error!", MB_ICONEXCLAMATION | MB_OK);
                            return FALSE;
                        }
                    }
                    break;
                case ID_MENU_STOP:
                    for (int i = 0; i < 6; ++i)
                    {
                        if (SuspendThread(threads[i]) == -1)
                        {
                            MessageBox(nullptr, L"Couldn't suspend a thread.",
                                L"Error!", MB_ICONEXCLAMATION | MB_OK);
                            return FALSE;
                        }
                    }
                    break;
            }
            break;
        case WM_GETMINMAXINFO:
        {
            MINMAXINFO* mmi = reinterpret_cast<MINMAXINFO*>(lParam);
            mmi->ptMinTrackSize.x = mmi->ptMaxTrackSize.x = WINDOW_WIDTH;
            mmi->ptMinTrackSize.y = mmi->ptMaxTrackSize.y = WINDOW_HEIGHT;
            break;
        }
        case WM_CLOSE:
            DestroyWindow(hWnd);
            break;
        case WM_DESTROY:
            PostQuitMessage(0);
            break;
        default:
            return DefWindowProc(hWnd, Msg, wParam, lParam);
    }

    return 0;
}

DWORD WINAPI ThreadProc(LPVOID lpParameter)
{
    ThreadData* data = reinterpret_cast<ThreadData*>(lpParameter);
    RECT rect = data->rect;

    int radius = min(rect.right - rect.left, rect.bottom - rect.top) / 2 -
        CIRCLE_RADIUS;

    POINT center = {
        (rect.right + rect.left) / 2,
        (rect.bottom + rect.top) / 2
    };

    int x;
    int y;

    float phi = -M_PI;
    float delta = M_PI / 50;

    int top, left, right, bottom;

    while (true)
    {
        phi += delta;

        if (phi > M_PI)
            phi -= 2 * M_PI;

        x = static_cast<int>(radius * cos(phi));
        y = static_cast<int>(radius * sin(phi));

        HDC hdc = GetDC(data->hwnd);

        left = center.x + x - CIRCLE_RADIUS;
        right = center.x + x + CIRCLE_RADIUS;
        top = center.y + y - CIRCLE_RADIUS;
        bottom = center.y + y + CIRCLE_RADIUS;

        FillRect(hdc, &rect, reinterpret_cast<HBRUSH>(COLOR_WINDOW));
        Ellipse(hdc, left, top, right, bottom);

        ReleaseDC(data->hwnd, hdc);
        Sleep(50);
    }
}
